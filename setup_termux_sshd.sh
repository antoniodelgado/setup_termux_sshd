#!/bin/bash -xe
port=2222

pkg install openssl
pkg install openssh
pkg install termux-auth
test -e "${PREFIX}/etc/ssh/ssh_host_ed25519_key" || ssh-keygen -b 4096 -f "${PREFIX}/etc/ssh/ssh_host_ed25519_key" -t ed25519 -N ""
echo "Port ${port}" >> "${PREFIX}/etc/ssh/sshd_config"
sshd
echo "pgrep sshd || sshd" >> "${HOME}/.profile"
echo "Create a strong password:"
passwd
echo "Now you have to add your personal public key to '${HOME}/.ssh/authorized_keys'"
echo "And then run:"
echo "sed -i 's/PasswordAuthentication yes/PasswordAuthentication no/' '${PREFIX}/etc/ssh/sshd_config'"
