## setup_termux_sshd

This script setup_termux_ssh.sh setup OpenSSH server to start when you open Termux (and remain open) in Android.
You need to install Termux https://termux.dev/, I suggest you to do it from F-Droid https://f-droid.org/ .

You can change the port to listen in the shell script.

Then add your public key to "${HOME}/.ssh/authorized_keys"

This script expand a bit and harden this https://wiki.termux.com/wiki/Remote_Access
